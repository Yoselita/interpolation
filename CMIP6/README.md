The script **CMIP6_remap.sh** was used to perform the interpolation of all montlhy CMPI6 ocean data (siconc, ph, and tos) data used in ATLAS to the common 1 degree grid.

**To run the script:**
	 
	 source CMIP6_Omon_remap.sh <variable name>

<varname> could be e.g. siconc, ph, or tos 

**Conservative interpolation for CMIP6 ocean model outputs:**
 The data are interpolated on 1 degree regular grid, using 1st order conservative remapping. 
 The Climate Data Operator (CDO) software is used for the inteprolation.
 The procedure:
 2 files used for this step:
 <details><summary>1. destination_mask_1: e.g. land_sea_mask_1degree.nc4
 <details><summary>2. source_file

	basic interpolation procedure done:
		cdo griddes [destination_mask] > destination.grid
		cdo cdo gencon,destination.grid [source_file] weights.nc
		cdo remap,destination.grid,weights.nc -selname,[varname] [source_file] file_interpolated.nc


**********************************************************************************************************************************
**Alternative procedure for interpolating CMIP6 ocean model outputs:**
Applied to interpoplate the outputs from the ocean models that have irregular grids which crashed due to lack of informations when using the basic conservative interpolation from above

 An alternative method has been applied to procede with conservative interpolation and keep the consistency in the process of 
 interpolation of CMIP6 ocean data. The data for each above listed model were interpolated on the finer grid (0.5 degree - 360x720) using 
 distance-weighted average remapping (remapdis) in order to get the data on the regular grid. We chose the finer grid in order to 
 lose as less information as possible.
Files used for this step:
<details><summary>1. destination_mask_05: e.g. land_sea_mask_05degree.nc4
<details><summary>2. destination_mask_1: e.g. land_sea_mask_1degree.nc4
<details><summary>3. source_file

	Interpolation procedure:
		cdo griddes [destination_mask_05] > destination_05degree.grid
		cdo griddes [destination_mask_1] > destination_1degree.grid
		cdo remapdis,destination_05degree.grid [source_file] intermediate_file.nc
		cdo gencon,destination_1degress.grid intermediate_file.nc weights.nc
		cdo remap,destination_1degress.grid,weights.nc -selname,tos intermediate_file.nc file_interpolated.nc

**********************************************************************************************************************************

**NOTE:**
Script also performs final masking of all the interpolated data to a common land-sea mask if needed. In that case, additional information on the full path to the common land-sea mask needs to be provided by a user. 
